import 'package:flutter/material.dart';

/// list item
class CryptoAssetItem extends StatelessWidget {
  /// initialization
  const CryptoAssetItem({Key? key, required this.url, required this.assetId})
      : super(key: key);

  /// url
  final String url;

  /// asset id
  final String assetId;

  @override
  Widget build(BuildContext context) {
    return Card(
        child: ListTile(
            leading:
                CircleAvatar(radius: 20, backgroundImage: NetworkImage(url)),
            title: Center(
                child: Text(assetId,
                    style: const TextStyle(fontWeight: FontWeight.bold)))));
  }
}
