import 'dart:convert';

import 'package:crypto_stats/core/models/market.dart';
import 'package:crypto_stats/core/repository/crypto_asset_repository.dart';
import 'package:crypto_stats/core/utils/constants.dart';
import 'package:crypto_stats/generated/assets.gen.dart';
import 'package:flutter/material.dart';

// ignore: public_member_api_docs
class LivePrices extends StatelessWidget {
  /// initialization
  LivePrices({Key? key}) : super(key: key);
  final List<MarketPoint> _markets = [];
  final Map<String, int> _keys = {};

  @override
  Widget build(BuildContext context) {
    final liveMarketStream = CryptoAssetRepository().getLiveMarketData();
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
        Widget>[
      const Text('Live Prices', style: TextStyle(fontSize: 18)),
      Container(
          height: 230,
          padding: const EdgeInsets.only(top: 10),
          child: StreamBuilder<dynamic>(
              stream: liveMarketStream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  final marketData = MarketPoint.fromJson(
                      jsonDecode(snapshot.data.toString())
                          as Map<String, dynamic>);
                  setCoinValue(marketData);
                  return ListView.builder(
                      physics: const BouncingScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      itemCount: _markets.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Padding(
                            padding: const EdgeInsets.only(right: 10),
                            child: Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              child: Container(
                                  width: 250,
                                  height: 230,
                                  padding:
                                      const EdgeInsets.fromLTRB(20, 10, 20, 10),
                                  child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Row(children: <Widget>[
                                          CircleAvatar(
                                              backgroundImage:
                                                  _markets[index].icon),
                                          const SizedBox(width: 20),
                                          Text.rich(TextSpan(children: [
                                            TextSpan(
                                                text: _markets[index].title,
                                                style: const TextStyle(
                                                    fontSize: 20,
                                                    fontWeight:
                                                        FontWeight.bold)),
                                            const TextSpan(
                                                text: CryptoCoins.usd,
                                                style: TextStyle(fontSize: 20))
                                          ])),
                                        ]),
                                        Text(_markets[index].price.toString(),
                                            style: const TextStyle(
                                                fontSize: 24,
                                                fontWeight: FontWeight.bold)),
                                        Text(_markets[index].takerSide ?? '',
                                            style:
                                                const TextStyle(fontSize: 16))
                                      ])),
                            ));
                      });
                } else {
                  return const Center(child: CircularProgressIndicator());
                }
              }))
    ]);
  }

  /// arrange items
  void setCoinValue(MarketPoint market) {
    switch (market.symbolId) {
      case CryptoCoins.btcUsd:
        market.title = 'BTC';
        if (_keys[market.symbolId] == null) {
          _markets.add(market..icon = Assets.images.btc);
          _keys[market.symbolId!] = _markets.length - 1;
        } else {
          _markets[_keys[market.symbolId]!] = market..icon = Assets.images.btc;
        }
        break;
      case CryptoCoins.ethUsd:
        market.title = 'ETH';
        if (_keys[market.symbolId] == null) {
          _markets.add(market..icon = Assets.images.eth);
          _keys[market.symbolId!] = _markets.length - 1;
        } else {
          _markets[_keys[market.symbolId]!] = market..icon = Assets.images.eth;
        }
        break;
      case CryptoCoins.adaUsd:
        market.title = 'ADA';
        if (_keys[market.symbolId] == null) {
          _markets.add(market..icon = Assets.images.ada);
          _keys[market.symbolId!] = _markets.length - 1;
        } else {
          _markets[_keys[market.symbolId]!] = market..icon = Assets.images.ada;
        }
        break;
    }
  }
}
