import 'package:crypto_stats/core/providers/asset.dart';
import 'package:crypto_stats/crypto_stats/view/widgets/crypto_asset_item.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

/// crypto assets list
class CryptoAssets extends StatelessWidget {
  /// constructor
  const CryptoAssets({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      Provider.of<Asset>(context, listen: false).provideCryptoAssets();
    });
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const Text('Crypto assets', style: TextStyle(fontSize: 18)),
          Container(
              height: 400,
              padding: const EdgeInsets.only(top: 20),
              child: Consumer<Asset>(builder: (context, asset, _) {
                if (asset.isLoading) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                } else {
                  final cryptoAssets = asset.cryptoAssets;
                  return ListView.builder(
                      shrinkWrap: true,
                      itemCount: cryptoAssets.length,
                      itemBuilder: (context, index) {
                        return CryptoAssetItem(
                            url: cryptoAssets[index].url!,
                            assetId: cryptoAssets[index].assetId!);
                      });
                }
              }))
        ]);
  }
}
