import 'package:crypto_stats/core/models/crypto_asset.dart';
import 'package:crypto_stats/core/utils/constants.dart';
import 'package:crypto_stats/core/utils/remote_client.dart';
import 'package:crypto_stats/core/utils/socket_client.dart';

/// base repository of all methods
abstract class BaseRepository {
  /// get all crypto assets
  Future<List<CryptoAsset>> getCryptoAssets();

  /// stream live data
  Stream getLiveMarketData();
}

/// connector of network and local data
class CryptoAssetRepository extends BaseRepository {
  @override
  Future<List<CryptoAsset>> getCryptoAssets() async {
    final result =
        await RestClient().getRequest<List<dynamic>>(ApiConstants.cryptoAssets);
    return result.data!
        .map((dynamic e) => CryptoAsset.fromJson(e as Map<String, dynamic>))
        .toList();
  }

  @override
  Stream getLiveMarketData() {
    return SocketClient().getLiveMarket();
  }
}
