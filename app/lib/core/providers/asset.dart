import 'package:crypto_stats/core/models/crypto_asset.dart';
import 'package:crypto_stats/core/repository/crypto_asset_repository.dart';
import 'package:flutter/foundation.dart';

/// asset provider
class Asset extends ChangeNotifier {
  final _repository = CryptoAssetRepository();
  final List<CryptoAsset> _cryptoAssets = [];

  /// manages loading
  bool isLoading = false;

  /// immutable crypto list
  List<CryptoAsset> get cryptoAssets => [..._cryptoAssets];

  /// provides assets
  Future<void> provideCryptoAssets() async {
    isLoading = true;
    notifyListeners();
    _cryptoAssets.clear();
    try {
      final data = await _repository.getCryptoAssets();
      _cryptoAssets.addAll(data);
    } catch (e) {
      isLoading = false;
      return;
    }
    isLoading = false;
    notifyListeners();
  }
}
