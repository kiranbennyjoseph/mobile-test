/// provide constants such as api keys
class ApiConstants {
  /// api key used to authenticate server
  static const apiKey = '17C49609-52DE-412C-979E-0CEB46451854';

  /// base url
  static const baseUrl = 'https://rest.coinapi.io';

  /// crypto assets end point
  static const cryptoAssets = '/v1/assets/icons/100';

  /// web socket url
  static const webSocketUrl = 'wss://ws-sandbox.coinapi.io/v1/';
}

/// currently monitoring coins
class CryptoCoins {
  // ignore: public_member_api_docs
  static const usd = '/USD';

  /// btc usd id
  static const btcUsd = 'COINBASE_SPOT_BTC_USD';

  /// eth usd id
  static const ethUsd = 'COINBASE_SPOT_ETH_USD';

  /// ada usd id
  static const adaUsd = 'COINBASE_SPOT_ADA_USD';
}
