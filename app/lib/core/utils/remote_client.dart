import 'package:dio/dio.dart';

import 'constants.dart';

/// basic remote connection provider
class RestClient {
  /// singleton initialization
  factory RestClient() {
    return _instance;
  }

  RestClient._privateConstructor();

  static final RestClient _instance = RestClient._privateConstructor();

  Dio? _dio;
  var _cancelToken = CancelToken();

  /// dio initialization
  void reInitDio() {
    if (_dio == null || _cancelToken.isCancelled) {
      _dio = Dio(BaseOptions(
          connectTimeout: 5000,
          receiveTimeout: 5000,
          baseUrl: ApiConstants.baseUrl,
          headers: <String, String>{
            'X-CoinAPI-Key': '{${ApiConstants.apiKey}}'
          }));
      _cancelToken = CancelToken();
    }
  }

  /// get request implementation
  Future<Response<T>> getRequest<T>(String urlEndpoint) {
    reInitDio();
    return _dio!.get<T>(urlEndpoint);
  }

  /// dio request cancel method
  void cancel() {
    _cancelToken.cancel();
    _dio = null;
  }
}
