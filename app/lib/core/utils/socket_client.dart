import 'dart:convert';

import 'package:crypto_stats/core/utils/constants.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

/// socket singleton class
class SocketClient {
  /// singleton initialization
  factory SocketClient() {
    return _instance;
  }

  SocketClient._privateConstructor();

  static final SocketClient _instance = SocketClient._privateConstructor();

  final WebSocketChannel _channel =
      WebSocketChannel.connect(Uri.parse(ApiConstants.webSocketUrl));

  /// gets realtime data as [Stream]
  Stream getLiveMarket() {
    _channel.sink.add(json.encode({
      'type': 'hello',
      'apikey': ApiConstants.apiKey,
      'heartbeat': false,
      'subscribe_data_type': ['trade'],
      'subscribe_filter_symbol_id': [
        r'COINBASE_SPOT_BTC_USD$',
        r'COINBASE_SPOT_ETH_USD$',
        r'COINBASE_SPOT_ADA_USD$'
      ]
    }));
    return _channel.stream;
  }

  /// closes current socket sink
  void close() {
    _channel.sink.close();
  }
}
