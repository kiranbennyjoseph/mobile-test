import 'package:crypto_stats/generated/assets.gen.dart';

class MarketPoint {
  // ignore: public_member_api_docs
  String? timeExchange;

  // ignore: public_member_api_docs
  String? timeCoinapi;

  // ignore: public_member_api_docs
  String? uuid;

  // ignore: public_member_api_docs
  String? price;

  // ignore: public_member_api_docs
  String? size;

  // ignore: public_member_api_docs
  String? takerSide;

  // ignore: public_member_api_docs
  String? symbolId;

  // ignore: public_member_api_docs
  String? type;

  // ignore: public_member_api_docs
  AssetGenImage? icon;

  // ignore: public_member_api_docs
  String? title;

  // ignore: public_member_api_docs
  MarketPoint(
      {this.timeExchange,
      this.timeCoinapi,
      this.uuid,
      this.price,
      this.size,
      this.takerSide,
      this.symbolId,
      this.type,
      this.icon,
      this.title});

// ignore: public_member_api_docs, sort_constructors_first
  MarketPoint.fromJson(Map<String, dynamic> json) {
    timeExchange = json['time_exchange'] as String;
    timeCoinapi = json['time_coinapi'] as String;
    uuid = json['uuid'] as String;
    price = json['price']?.toString();
    size = json['size']?.toString();
    takerSide = json['taker_side'] as String;
    symbolId = json['symbol_id'] as String;
    type = json['type'] as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['time_exchange'] = timeExchange;
    map['time_coinapi'] = timeCoinapi;
    map['uuid'] = uuid;
    map['price'] = price;
    map['size'] = size;
    map['taker_side'] = takerSide;
    map['symbol_id'] = symbolId;
    map['type'] = type;
    return map;
  }
}
