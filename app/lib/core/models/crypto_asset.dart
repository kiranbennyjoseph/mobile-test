/// asset_id : "BTC"
/// url : "https://s3.eu-central-1.amazonaws.com/bbxt-static-icons/type-id/png_512/4caf2b16a0174e26a3482cea69c34cba.png"

class CryptoAsset {
  String? assetId;
  String? url;

  CryptoAsset({this.assetId, this.url});

  CryptoAsset.fromJson(dynamic json) {
    assetId = json['asset_id'] as String;
    url = json['url'] as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['asset_id'] = assetId;
    map['url'] = url;
    return map;
  }
}
