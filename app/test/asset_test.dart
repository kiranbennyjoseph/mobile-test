import 'package:crypto_stats/core/providers/asset.dart';
import 'package:crypto_stats/crypto_stats/view/widgets/crypto_assets.dart';
import 'package:crypto_stats/crypto_stats/view/widgets/profile.dart';
import 'package:crypto_stats/generated/assets.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:provider/provider.dart';

void main() {
  // Define a test. The TestWidgets function also provides a WidgetTester
  // to work with. The WidgetTester allows building and interacting
  // with widgets in the test environment.
  testWidgets('Profile widget has a name', (WidgetTester tester) async {
    // Create the widget by telling the tester to build it.
    await tester.pumpWidget(MaterialApp(
      home: Profile(
        name: 'test name',
        message: 'Welcome',
        avatar: Assets.images.avatar,
      ),
    ));

    // Create the Finders.
    final nameFinder = find.text('test name');

    // Use the `findsOneWidget` matcher provided by flutter_test to
    // verify that the Text widgets appear exactly once in the widget tree.
    expect(nameFinder, findsOneWidget);
  });

  testWidgets('test if provider works correctly', (WidgetTester tester) async {
    final _providerKey = GlobalKey();
    late BuildContext mContext;
    // Create the widget by telling the tester to build it.
    await tester.pumpWidget(MaterialApp(
      home: MultiProvider(
        key: _providerKey,
        providers: [
          ChangeNotifierProvider.value(value: Asset()),
        ],
        child: Builder(builder: (context) {
          mContext = context;
          return const CryptoAssets();
        }),
      ),
    ));
    expect(Provider.of<Asset>(mContext, listen: false).cryptoAssets, isEmpty);

    /// allow disposing of timers
    await tester.pumpAndSettle(const Duration(seconds: 5));
  });
}
